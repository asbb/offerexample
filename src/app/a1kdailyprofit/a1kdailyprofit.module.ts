import { HtmlService } from './services/html.service';
import { AuthGuardService } from './../shared/services/auth-guard.service';
import { VocabularyResolverService } from './../shared/services/vocabulary.resolver.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptInComponent } from './components/opt-in/opt-in.component';
import { RegFormComponent } from './components/reg-form/reg-form.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

export const routes: Routes = [
  {
    path: '',
    component: OptInComponent,
    resolve: {
      resolvedVocabulary: VocabularyResolverService
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    resolve: {
      resolvedVocabulary: VocabularyResolverService
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule.forRoot(),
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [OptInComponent, RegFormComponent, SignUpComponent],
  providers: [
    HtmlService
  ]
})
export class A1kdailyprofitModule { }
