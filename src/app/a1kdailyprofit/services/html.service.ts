import { GeneralService } from './../../shared/services/general.service';
import { environment } from './../../../environments/environment';
import { DOCUMENT } from '@angular/common';
import { Injectable, Inject, Renderer2, RendererFactory2 } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class HtmlService {
  private renderer: Renderer2;
  constructor(
    @Inject(DOCUMENT) private _document: HTMLDocument,
    public rendererFactory: RendererFactory2,
    public general: GeneralService
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  initStyles() {
    if (!this._document.getElementsByClassName('customStyle').length) {
      const stylescss = this.renderer.createElement('link');
      const fontawesome = this.renderer.createElement('link');
      const formscss = this.renderer.createElement('link');
      const formCustom = this.renderer.createElement('link');
      const reset = this.renderer.createElement('link');
      const fonts = this.renderer.createElement('link');
      const select2 = this.renderer.createElement('link');

      fonts.setAttribute('rel', 'stylesheet');
      fonts.setAttribute('class', 'customStyle');
      fonts.setAttribute('href', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');

      formscss.setAttribute('rel', 'stylesheet');
      formscss.setAttribute('class', 'customStyle');
      formscss.setAttribute('type', 'text/css');
      formscss.setAttribute('href', '/assets/a1kdailyprofit/css/form-css.css');

      select2.setAttribute('rel', 'stylesheet');
      select2.setAttribute('class', 'customStyle');
      select2.setAttribute('type', 'text/css');
      select2.setAttribute('src', '/assets/a1kdailyprofit/js/select2/css/select2.min.css');

      reset.setAttribute('rel', 'stylesheet');
      reset.setAttribute('class', 'customStyle');
      reset.setAttribute('type', 'text/css');
      reset.setAttribute('href', '/assets/a1kdailyprofit/css/reset.css');

      formCustom.setAttribute('rel', 'stylesheet');
      formCustom.setAttribute('class', 'customStyle');
      formCustom.setAttribute('type', 'text/css');
      formCustom.setAttribute('href', '/assets/a1kdailyprofit/css/form-css-1k.css');

      stylescss.setAttribute('rel', 'stylesheet');
      stylescss.setAttribute('class', 'customStyle');
      stylescss.setAttribute('type', 'text/css');
      stylescss.setAttribute('href', '/assets/a1kdailyprofit/css/style.css');

      this._document.head.appendChild(reset);
      this._document.head.appendChild(fonts);
      this._document.head.appendChild(stylescss);
      this._document.head.appendChild(formscss);
      this._document.head.appendChild(formCustom);
    } else {
      const collection: any = this._document.getElementsByClassName('customStyle');
      for (const i in collection) {
        if (collection[0]) {
          collection[0].parentNode.removeChild(collection[0]);
        }
      }
      this.initStyles();
    }
  }

  initScripts() {
    if (!this._document.getElementsByClassName('customScript').length) {
      const scriptsjs = this.renderer.createElement('script');
      const exitjs = this.renderer.createElement('script');
      const select2 = this.renderer.createElement('script');
      const jqueryzclip = this.renderer.createElement('script');
      const custom = this.renderer.createElement('script');

      custom.setAttribute('type', 'text/javascript');
      custom.setAttribute('class', 'customScript');
      custom.setAttribute('src', '/assets/a1kdailyprofit/js/custom.js');

      jqueryzclip.setAttribute('type', 'text/javascript');
      jqueryzclip.setAttribute('class', 'customScript');
      jqueryzclip.setAttribute('src', '/assets/a1kdailyprofit/js/jquery.zclip.js');

      select2.setAttribute('type', 'text/javascript');
      select2.setAttribute('class', 'customScript');
      select2.setAttribute('src', '/assets/a1kdailyprofit/js/select2/js/select2.min.js');

      scriptsjs.setAttribute('type', 'text/javascript');
      scriptsjs.setAttribute('class', 'customScript');
      scriptsjs.setAttribute('src', '/assets/a1kdailyprofit/js/scripts.js');

      exitjs.setAttribute('type', 'text/javascript');
      exitjs.setAttribute('class', 'customScript');
      exitjs.setAttribute('src', '/assets/a1kdailyprofit/js/exits.js');

      this._document.body.appendChild(jqueryzclip);
      this._document.body.appendChild(select2);
      this._document.body.appendChild(scriptsjs);
      this._document.body.appendChild(exitjs);
      this._document.body.appendChild(custom);
    } else {
      const collection: any = this._document.getElementsByClassName('customScript');
      for (const i in collection) {
        if (collection[0]) {
          collection[0].parentNode.removeChild(collection[0]);
        }
      }
      this.initScripts();
    }
  }
}
