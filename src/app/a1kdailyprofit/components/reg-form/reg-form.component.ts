import { BasicRegFormComponent } from './../../../shared/components/basic-reg-form/basic-reg-form.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'reg-form',
  templateUrl: '../../../shared/components/basic-reg-form/basic-reg-form.component.html',
  styleUrls: ['./reg-form.component.scss']
})
export class RegFormComponent extends BasicRegFormComponent {}
