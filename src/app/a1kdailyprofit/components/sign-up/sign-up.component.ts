import { VocabularyStorageService } from './../../../vocabulary-storage.service';
import { GoogleAnalyticsEventsService } from './../../../shared/services/google-analytics-events.service';
import { environment } from './../../../../environments/environment';
import { HtmlService } from './../../services/html.service';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from './../../../shared/services/general.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BasicSignUpComponent } from './../../../shared/components/basic-sign-up/basic-sign-up.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent extends BasicSignUpComponent implements OnInit {

  constructor(
    public modalService: BsModalService,
    public service: GeneralService,
    public route: ActivatedRoute,
    public router: Router,
    public fb: FormBuilder,
    public ga: GoogleAnalyticsEventsService,
    public vocStorage: VocabularyStorageService,
    private _htmlService: HtmlService
  ) {
    super(modalService, service, route, fb, router, ga, vocStorage);
    this._htmlService.initStyles();
    this._htmlService.initScripts();
  }

  ngOnInit() {
    const lang = `_${this.service.language.toUpperCase()}`;
    const videoNumber = '2_360';
    this.vocabulary = this.route.snapshot.data['resolvedVocabulary'];
    this.videoUrl = `${this.protocol === 'http:' ? '' : this.protocol}//1kdailyprofit.me/video.php?link=1Kdaily${lang}_${videoNumber}&autoplay=${this.service.videoAutoplay}&p=2`;

    if (this.service.optinForm) {
      const fname = this.service.optinForm.get('name').value,
        email = this.service.optinForm.get('email').value;
      this.pixelUrl = `https://1kdailyprofit.me/opt/${this.service.language}/index.php?ot=${environment.offerToken}&lang=${this.service.language}&fname=${fname}&email=${email}&clicker-token=${this.service.clickerToken}&clickid=${this.service.queryParams['clickid'] || ''}&aff=${this.service.queryParams['aff'] || ''}`;
      this.queryParams.forEach((param: string) => {
        if (this.service.queryParams.param) {
          this.pixelUrl = this.pixelUrl + `&${param}=${this.service.queryParams.param}`;
        }
      });
    }
  }

}
