import { VocabularyStorageService } from './../../../vocabulary-storage.service';
import { HtmlService } from './../../services/html.service';
import { DOCUMENT } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { GeneralService } from './../../../shared/services/general.service';
import { BasicOptInComponent } from './../../../shared/components/basic-opt-in/basic-opt-in.component';
import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-opt-in',
  templateUrl: './opt-in.component.html',
  styleUrls: [
    './opt-in.component.scss',
    '../../../../assets/a1kdailyprofit/css/style.css'
  ]
})
export class OptInComponent extends BasicOptInComponent implements OnInit {
  constructor(
    public service: GeneralService,
    public modalService: BsModalService,
    public route: ActivatedRoute,
    public fb: FormBuilder,
    public router: Router,
    private _htmlService: HtmlService,
    public vocStorage: VocabularyStorageService,
    meta: Meta,
    title: Title
  ) {
    super(service, modalService, route, fb, router, vocStorage);
    title.setTitle('My opt-in page');
    meta.addTags([
      { name: 'author', content: '1kdailyprofit' },
      { name: 'description', content: 'jsdnfjsndfjnsdfjn!' }
    ]);
    this._htmlService.initStyles();
    this._htmlService.initScripts();
  }

  ngOnInit() {
    const videoNumber = '1';
    this.vocabulary = this.route.snapshot.data['resolvedVocabulary'];
    if (this.service.language === 'en') {
      const lang = '';
      this.videoUrl = `${this.protocol === 'http:' ? '' : this.protocol}//1kdailyprofit.me/video.php?link=1kVideo1F_${videoNumber}${lang}_360&autoplay=${this.service.videoAutoplay}&p=2`;
    } else {
      const lang =  '_' + this.service.language.toUpperCase();
      this.videoUrl = `${this.protocol === 'http:' ? '' : this.protocol}//1kdailyprofit.me/video.php?link=1Kdaily${lang}_${videoNumber}_360&autoplay=${this.service.videoAutoplay}&p=2`;
    }
  }
}
