import { VocabularyResolverService } from './shared/services/vocabulary.resolver.service';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
import { ModuleWithProviders } from '@angular/core';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: `/en`,
    pathMatch: 'full'
  },
  {
    path: ':lang',
    loadChildren: `./${environment.offer}/${environment.offer}.module#${environment.module}Module`
  }
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
