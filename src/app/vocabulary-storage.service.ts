import { Injectable } from '@angular/core';

@Injectable()
export class VocabularyStorageService {
  public vocabulary: any;
  public continent: string;

  constructor() {
    this.continent = '';
  }

}
