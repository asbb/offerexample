import { VocabularyStorageService } from './vocabulary-storage.service';
import { environment } from './../environments/environment';
import { Title, Meta } from '@angular/platform-browser';
import { Router, NavigationStart } from '@angular/router';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer, DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public json1: any;
  public json2: any;
  public json3: any;
  public prerenderContent: boolean;
  public env: any;
  public showCookiePolicy: boolean;
  public languages: string[] = [
    'de',
    'en',
    'es',
    'it',
    'no',
    'ru',
    'se'
  ];

  constructor(
    public meta: Meta,
    public title: Title,
    public router: Router,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private renderer: Renderer2,
    public vocStorage: VocabularyStorageService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    const favicon = this.renderer.createElement('link');
    this.env = environment;
    favicon.setAttribute('rel', 'icon');
    favicon.setAttribute('type', 'image/x-icon');
    favicon.setAttribute('href', this.env.favicon);
    this._document.head.appendChild(favicon);
    this.prerenderContent = true;

    if (isPlatformBrowser(this.platformId)) {
      this.showCookiePolicy = localStorage.getItem('CookiePolicyClosed') === null;
    }
  }

  onCloseCookiePolicy() {
    localStorage.setItem('CookiePolicyClosed', 'true');
    this.showCookiePolicy = false;
  }

  ngOnInit() {
  }
}
