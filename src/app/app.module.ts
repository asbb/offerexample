import { VocabularyStorageService } from './vocabulary-storage.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutes, AppRouting } from './app.routing';
import { RouterModule } from '@angular/router';
import { NgxJsonLdModule } from 'ngx-json-ld';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRouting,
    NgxJsonLdModule,
    BrowserModule.withServerTransition({ appId: 'offers' }),
  ],
  providers: [VocabularyStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
