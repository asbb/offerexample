import { TestBed, inject } from '@angular/core/testing';

import { VocabularyStorageService } from './vocabulary-storage.service';

describe('VocabularyStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VocabularyStorageService]
    });
  });

  it('should be created', inject([VocabularyStorageService], (service: VocabularyStorageService) => {
    expect(service).toBeTruthy();
  }));
});
