import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicSignUpComponent } from './basic-sign-up.component';

describe('BasicSignUpComponent', () => {
  let component: BasicSignUpComponent;
  let fixture: ComponentFixture<BasicSignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicSignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicSignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
