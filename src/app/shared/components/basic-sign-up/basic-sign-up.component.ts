import { VocabularyStorageService } from './../../../vocabulary-storage.service';
import { Observable } from 'rxjs/Observable';
import { GoogleAnalyticsEventsService } from './../../services/google-analytics-events.service';
import { environment } from './../../../../environments/environment';
import { finalize, timeInterval } from 'rxjs/operators';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BasicComponent } from './../basic/basic.component';
import { GeneralService, RegResp } from './../../services/general.service';
import { Component, OnInit, HostListener, ViewChild, Inject, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-basic-sign-up',
  templateUrl: './basic-sign-up.component.html',
  styleUrls: ['./basic-sign-up.component.scss']
})
export class BasicSignUpComponent extends BasicComponent implements OnInit, AfterViewInit {
  @ViewChild('techfinForm') public techfinForm;
  public queryParams: string[] = [
    'clickid',
    'aff',
    'affid',
    'p3',
    'p4'
  ];
  public vocabulary: any;
  public modalRef: BsModalRef;
  public videoUrl: string;
  public pixelUrl: string;
  public isLoading: boolean;
  public brokeReason: string;
  public regForm: FormGroup;
  public regErr: any = {};
  public city: string;
  public country: string;
  public techfinUrl: string;

  constructor(
    public modalService: BsModalService,
    public service: GeneralService,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _router: Router,
    private _ga: GoogleAnalyticsEventsService,
    private _vocStorage: VocabularyStorageService
  ) {
    super(modalService, service);
    this.brokeReason = '';
    this.regForm = this._fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      phone_code: ['', Validators.required],
      phone_number: ['', Validators.required],
      language: [navigator.language],
      clicker_token: [service.clickerToken],
      offer_token: [environment.offerToken],
      clickid: [this.service.queryParams['clickid'] || ''],
      aff: [this.service.queryParams['aff'] || ''],
      affid: [this.service.queryParams['affid'] || ''],
      p3: [this.service.queryParams['p3'] || ''],
      p4: [this.service.queryParams['p4'] || ''],
      ip: [this.service.queryParams['ip'] || ''],
      br: [this.service.queryParams['br'] || ''],
    });

    this.service.getGeoData().subscribe((res) => {
      this.city = res.city;
      this.country = res.country;
      this.regForm.get('phone_code').setValue(res.phone_code);
      if (res.continent) {
        this._vocStorage.continent = res.continent;
      }
    });

    window.scrollTo(0, 0);
  }

  registrationLoop(data: { success: boolean, job_url: string }, form: FormGroup) {
    setTimeout(() => {
      this.service.registrateStep2(data.job_url).subscribe(
        (res: RegResp) => {
          switch (res.status) {
            case 3:
              this._ga.emitEvent('Landing', 'Registration', 'Success');
              if (!this.service.disableRedirect) {
                if (res.softwareplatform === 'techfin') {
                  this.techfinUrl = res.redirect_url;
                  this.techfinForm.submit();
                } else {
                  window.location.href = res.redirect_url;
                }
              }
              break;
            case -1:
              this.isLoading = false;
              this.brokeReason = res.message;
              console.log(this.brokeReason);
              break;

            default:
              this.registrationLoop(data, form);
              break;
          }
        }
      );
    }, 2000);
  }

  onSubmit(form: FormGroup) {
    if (!this.isLoading && form instanceof FormGroup) {
      this._ga.emitEvent('Landing', 'Registration', 'Attempt');
      this.regErr = {};
      this.regForm.get('clicker_token').setValue(this.service.getClickerToken());

      if (!form.valid) {
        const keys = Object.keys(form.value);
        keys.forEach((key) => {
          if (form.get(key).invalid) {
            form.get(key).markAsTouched();
          }
        });
      } else {
        this.isLoading = true;
        this.service.registrateStep1(form.value)
          .subscribe(
            (res: { success: boolean, job_url: string }) => {
              this.registrationLoop(res, form);
            },
            (res) => {
              this.isLoading = false;
              if (res.status === 422) {
                res.error.forEach((err: any) => {
                  this.regErr[err.field] = err.message;
                });
              } else if (res.status) {
                console.error(res);
              }
            }
          );
      }
    }
  }

  ngAfterViewInit() {
    if (this.service.optinForm) {
      const fname = this.service.optinForm.get('name').value;
      const email = this.service.optinForm.get('email').value;
      this.queryParams.forEach((param: string) => {
        if (this.service.queryParams.param) {
          this.pixelUrl = this.pixelUrl + `&${param}=${this.service.queryParams.param}`;
        }
      });
      this.pixelUrl = `/opt/index.php?ot=${environment.offerToken}&lang=${this.service.language}&fname=${fname}&email=${email}&clicker-token=${this.service.clickerToken}`;
      this.service.addIframe(this.pixelUrl);
    }
  }
}
