import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicRegFormComponent } from './basic-reg-form.component';

describe('BasicRegFormComponent', () => {
  let component: BasicRegFormComponent;
  let fixture: ComponentFixture<BasicRegFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicRegFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicRegFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
