import { environment } from './../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralService } from './../../services/general.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'basic-reg-form',
  templateUrl: './basic-reg-form.component.html',
  styleUrls: []
})
export class BasicRegFormComponent implements OnInit {
  @Output() public submit = new EventEmitter<any>();
  @Input() public vocabulary: any;
  @Input() public techfinUrl: string; // url for submitting spec form in case of techfin software platform
  @Input() public isLoading: boolean; // flag to show/hide preloader over registration form
  @Input() public brokeReason: string = ''; // error info
  @Input() public enableTitle: boolean; // show/hide form title
  @Input() public regErr: any = {}; // registration fields errors that comes from the backend side
  @Input() public id: string | number = 0; // id for <form></form>
  @Input() public classes: string = 'top-form-wrapper member-form scroll-trigger reg-form'; // classes for dynamic conf
  @Input() public regForm: FormGroup = this._fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    phone_code: ['', Validators.required],
    phone_number: ['', Validators.required],
    language: [navigator.language],
    clicker_token: [this.service.clickerToken],
    offer_token: [environment.offerToken],
    clickid: [this.service.queryParams['clickid'] || ''],
    aff: [this.service.queryParams['aff'] || ''],
    affid: [this.service.queryParams['affid'] || ''],
    p3: [this.service.queryParams['p3'] || ''],
    ip: [this.service.queryParams['ip'] || ''],
    br: [this.service.queryParams['br'] || '']
  }); // stack of basic fields
  public focusedField: string;

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    public service: GeneralService
  ) {
  }

  ngOnInit() {
    if (this.service.optinForm) {
      const fname = this.service.optinForm.get('name').value,
        email = this.service.optinForm.get('email').value;
    }
    if (this.service.optinForm) {
      /**It means that form was filled at `opt-in` page*/
      const fname = this.service.optinForm.get('name').value,
        email = this.service.optinForm.get('email').value;
      this.regForm.get('email').setValue(email);
      this.regForm.get('firstname').setValue(fname);
    } else {
      this.regForm.get('email').setValue(this.service.queryParams['email'] || '');
      this.regForm.get('firstname').setValue(this.service.queryParams['name'] || '');
    }
  }

  onSubmit(form: FormGroup) {
    this.submit.emit(form);
  }
}
