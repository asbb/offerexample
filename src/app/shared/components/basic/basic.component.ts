import { GeneralService } from './../../services/general.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {
  @ViewChild('disclamerModal') public disclamerModal: any;
  @ViewChild('tncMosal') public tncModal: any;
  public tncModel: BsModalRef;
  public disclamerModel: BsModalRef;
  public beforeLeaveModel: BsModalRef;
  public mouseLeaveModel: BsModalRef;
  public mouseEnterTime: Date = null;
  public mouseLeaved: boolean;
  public protocol: string;
  public tncClasses: string = 'gray modal-fullscreen';
  public disclamerClasses: string = 'gray modal-fullscreen';

  constructor(
    public modalService: BsModalService,
    public service: GeneralService
  ) {
    this.protocol = window.location.protocol;
  }

  openTnc() {
    this.tncModel = this.modalService.show(
      this.tncModal,
      Object.assign({}, { class: this.tncClasses })
    );
  }

  hideModal(type: string = '') {
    switch (type) {
      case 'tnc':
        this.tncModel.hide();
        break;
      case 'disclamer':
        this.disclamerModel.hide();
        break;
      case 'mouseleave':
        this.mouseLeaveModel.hide();
        break;
      default:
        this.beforeLeaveModel.hide();
        break;
    }
  }

  openDisclamer() {
    this.disclamerModel = this.modalService.show(
      this.disclamerModal,
      Object.assign({}, { class: this.disclamerClasses })
    );
  }

  ngOnInit() {
  }

}
