import { VocabularyStorageService } from './../../../vocabulary-storage.service';
import { environment } from './../../../../environments/environment';
import { finalize, takeUntil } from 'rxjs/operators';
import { BasicComponent } from './../basic/basic.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GeneralService } from './../../services/general.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-basic-opt-in',
  templateUrl: './basic-opt-in.component.html',
  styleUrls: ['./basic-opt-in.component.scss']
})
export class BasicOptInComponent extends BasicComponent implements OnInit, OnDestroy {
  @ViewChild('modalBeforeLeave') public beforeLeaveModal: BsModalRef;
  @ViewChild('mouseLeaveModal') public mouseLeaveModal: BsModalRef;
  public vocabulary: any;
  public videoUrl: string;
  public loading: boolean;
  public city: string;
  public country: string;
  public optInForm: FormGroup;
  public regErr: any = {};
  private componentAlive$ = new Subject();
  @HostListener('window:unload', ['$event']) unloadHandler($event) {
    if (this.service.showLeaveModals && this.beforeLeaveModal) {
      this.beforeLeaveModel = this.modalService.show(
        this.beforeLeaveModal,
        Object.assign({}, { class: 'gray modal-mouseleave exit' })
      );
    }
    $event.returnValue = 'Are you sure?';
  }

  @HostListener('window:beforeunload', ['$event']) beforeunloadHandler($event) {
    if (this.service.showLeaveModals && this.beforeLeaveModal) {
      this.beforeLeaveModel = this.modalService.show(
        this.beforeLeaveModal,
        Object.assign({}, { class: 'gray modal-mouseleave exit' })
      );
    }
    $event.returnValue = 'Are you sure?';
  }

  @HostListener('mouseleave') onMouseLeave() {
    const date: Date = new Date,
      diff = this.mouseEnterTime ? (date.getTime() - this.mouseEnterTime.getTime()) / 1000 : 5;
    if (diff >= 5 && !this.modalService.getModalsCount() && this.service.showLeaveModals && this.mouseLeaveModal) {
      this.mouseLeaveModel = this.modalService.show(
        this.mouseLeaveModal,
        Object.assign({}, { class: 'gray modal-mouseleave exit-background' })
      );
      this.mouseLeaved = true;
    }
  }

  @HostListener('mouseenter') onMouseEnter() {
    if (this.mouseLeaved) {
      this.mouseEnterTime = new Date();
      this.mouseLeaved = false;
    }
  }

  constructor(
    public service: GeneralService,
    public modalService: BsModalService,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _router: Router,
    private _vocStorage: VocabularyStorageService
  ) {
    super(modalService, service);
    this.optInForm = this._fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      offer_token: [environment.offerToken, Validators.required],
    });
    Object.keys(this.optInForm.controls).forEach((name: string) => {
      this.formProcessing(
        this.optInForm.get(name),
        name
      );
    });
    this.service.getGeoData().subscribe((res) => {
      this.city = res.city;
      this.country = res.country;
      if (res.continent) {
        this._vocStorage.continent = res.continent;
      }
    });
  }

  formProcessing(control: FormControl | any, name: string) {
    control.valueChanges
      .pipe(
        takeUntil(this.componentAlive$)
      )
      .subscribe((res) => {
        if (this.regErr[name]) {
          delete this.regErr[name];
        }
      });

  }

  ngOnDestroy() {
    this.componentAlive$.complete();
  }

  hideModal(type: string) {
    if (this.modalService.getModalsCount()) {
      switch (type) {
        case 'tnc':
          this.tncModel.hide();
          break;
        case 'disclamer':
          this.disclamerModel.hide();
          break;
        case 'mouseleave':
          this.mouseLeaveModel.hide();
          break;
        default:
          this.beforeLeaveModel.hide();
          break;
      }
    }
  }

  onSubmit(form: FormGroup) {
    if (!this.loading) {
      if (!Object.keys(this.regErr).length) {
        this.regErr = {};
        this.loading = true;
        this.service.optinForm = this.optInForm;
        this.service.optIn(form.value)
          .pipe(
            finalize(() => this.loading = false)
          )
          .subscribe(
            () => {
              this.service.optinForm = form;
              this.hideModal('');
              this._router.navigate(
                ['sign-up'],
                {
                  relativeTo: this._route,
                  queryParams: {
                    ...this.service.queryParams,
                    ...{
                      'name': form.get('name').value,
                      'email': form.get('email').value
                    }
                  }
                }
              );
            },
            (res) => {
              this.loading = false;
              if (res.status === 422) {
                res.error.forEach((err: any) => {
                  this.regErr[err.field] = err.message;
                });
              } else if (res.status) {
                console.error(res);
              }
            }
          );
      }
    }
  }

  ngOnInit() {
    const lang = this.service.language !== 'en' ? '_' + this.service.language : '';
    const videoNumber = this.service.language !== 'en' ? '1' : '01';
    this.vocabulary = this._route.snapshot.data['resolvedVocabulary'];
    this.videoUrl = `${this.protocol === 'http:' ? '' : this.protocol}//thebitcoincode.com/video.php?link=bitcoin_${videoNumber}${lang}&poster=poster1&autoplay=${this.service.videoAutoplay}&p=2`;
  }

}
