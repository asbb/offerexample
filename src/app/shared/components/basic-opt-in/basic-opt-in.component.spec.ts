import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicOptInComponent } from './basic-opt-in.component';

describe('BasicOptInComponent', () => {
  let component: BasicOptInComponent;
  let fixture: ComponentFixture<BasicOptInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicOptInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicOptInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
