import { GoogleAnalyticsEventsService } from './services/google-analytics-events.service';
import { AuthGuardService } from './services/auth-guard.service';
import { GeneralService } from './services/general.service';
import { VocabularyResolverService } from './services/vocabulary.resolver.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicSignUpComponent } from './components/basic-sign-up/basic-sign-up.component';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BasicOptInComponent } from './components/basic-opt-in/basic-opt-in.component';
import { SafePipe } from './pipes/safe.pipe';
import { BasicComponent } from './components/basic/basic.component';
import { HttpClientModule } from '@angular/common/http';
import { BasicRegFormComponent } from './components/basic-reg-form/basic-reg-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    ModalModule.forRoot(),
    ReactiveFormsModule
  ],
  declarations: [
    BasicSignUpComponent,
    BasicOptInComponent,
    SafePipe,
    BasicComponent,
    BasicRegFormComponent
  ],
  exports: [
    SafePipe,
    BasicRegFormComponent
  ]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        VocabularyResolverService,
        GoogleAnalyticsEventsService,
        GeneralService,
        AuthGuardService
      ]
    };
  }
}
