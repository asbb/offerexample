import { TestBed, inject } from '@angular/core/testing';

import { VocabularyResolverService } from './vocabulary.resolver.service';

describe('VocabularyResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VocabularyResolverService]
    });
  });

  it('should be created', inject([VocabularyResolverService], (service: VocabularyResolverService) => {
    expect(service).toBeTruthy();
  }));
});
