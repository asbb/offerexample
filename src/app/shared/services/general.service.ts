import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { FormGroup } from '@angular/forms';
import { Injectable, Inject, Renderer2, RendererFactory2 } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Params } from '@angular/router';
import { DOCUMENT } from '@angular/common';

export interface RegResp {
  success: boolean;
  status: number;
  redirect_url?: string;
  message?: string;
  softwareplatform: string;
}

@Injectable()
export class GeneralService {
  public cachedOfferToken: string;
  public queryParams: any;
  public videoAutoplay: number;
  public optinForm: FormGroup = null;
  public optInAccept: boolean = null;
  public widgetParams: any = {};
  public showLeaveModals: number;
  public disableRedirect: boolean;
  public language: string; // current language
  public clickerToken: string = null;
  private renderer: Renderer2;

  constructor(
    private _http: HttpClient,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    public rendererFactory: RendererFactory2,
  ) {
    const optIn = this.cookieEnabled() ? localStorage.getItem('step2') : null;
    const date = new Date();

    this.renderer = rendererFactory.createRenderer(null, null);
    this.optInAccept = optIn !== null ? optIn === 'true' : true;
  }

  addIframe(src: string) {
    const wrapper = this._document.getElementById('wrapper');
    const iframe = this.renderer.createElement('iframe');

    iframe.setAttribute('src', src);
    iframe.setAttribute('width', '1');
    iframe.setAttribute('height', '1');

    wrapper.appendChild(iframe);
  }

  setOptIn(optIn: string) {
    if (this.optInAccept === null) {
      this.optInAccept = optIn ? +optIn !== 1 : true;
    } else if (optIn) {
      this.optInAccept = +optIn !== 1;
    }

    if (this.cookieEnabled()) {
      localStorage.setItem('step2', this.optInAccept.toString());
    }
  }

  processingQueryParams(params: Params) {
    const tokenQueryParam: string = params['token'];
    this.queryParams = params;
    this.videoAutoplay = params['autoplay'] ? +params['autoplay'] : 1;
    this.showLeaveModals = params['exit'] ? +(params['exit'].toLowerCase() !== 'no') : 1;
    this.disableRedirect = params['disableRedirect'] ? +params['disableRedirect'] === 1 : false;
    this.widgetParams = { /** params to enable/disable header widgets */
      w1: params['wgt1'] ? +(params['wgt1'].toLowerCase() !== 'no') : 1,
      w2: params['wgt2'] ? +(params['wgt2'].toLowerCase() !== 'no') : 1,
      showWidgets: params['wgt'] ? +(params['wgt'].toLowerCase() !== 'no') : 1
    };

    if (tokenQueryParam && tokenQueryParam.length) {
      this.setToken(tokenQueryParam);
    } else if (!this.clickerToken) {
      this.getToken().subscribe((res) => this.setToken(res.token));
    }
  }

  optIn(params: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Clicker-Token': this.getClickerToken()
      })
    };
    return this._http.post(
      `${environment.apiUrl}/user/auth/opt-in`,
      params, httpOptions
    );
  }

  registrateStep1(params: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Clicker-Token': this.getClickerToken()
      })
    };
    return this._http.post<{ success: boolean, job_url: string }>(
      `${environment.apiUrl}/user/auth/register`,
      params, httpOptions
    );
  }

  registrateStep2(url: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Clicker-Token': this.getClickerToken()
      })
    };
    return this._http.get<RegResp>(
      url,
      httpOptions
    );
  }

  getGeoData() {
    return this._http.get<{ city: string, country: string, phone_code: string, continent?: string }>(
      `${environment.apiUrl}/user/auth/geo-data`
    );
  }

  getVocabulary(lang: string) {
    return this._http.get<any>(
      `./assets/${environment.offer}/translations/vocabulary_${lang}.json`
    );
  }

  getClickerToken(): string {
    return this.cookieEnabled() ? localStorage.getItem('token') : this.clickerToken;
  }

  setToken(token: string) {
    if (this.cookieEnabled()) {
      localStorage.setItem('token', token);
    }
    this.clickerToken = token;
  }

  getToken() {
    return this._http.post<{ token: string }>(
      `${environment.apiUrl}/user/auth/token`,
      {}
    );
  }

  cookieEnabled() {
    this._document.cookie = 'cookietest=1';
    const ret = this._document.cookie.indexOf('cookietest=') !== -1;
    this._document.cookie = 'cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT';
    return ret;
  }
}
