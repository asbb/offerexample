import { environment } from './../../../environments/environment';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { GeneralService } from './general.service';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuardService {

  constructor(
    private _service: GeneralService,
    private _router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const languages = environment.languages;
    const lang = route.params['lang'] || 'en';

    this._service.setOptIn(route.queryParams['step2']);

    return Observable.create(observer => {
      if ((state.url.indexOf('/sign-up') === -1) && !this._service.optInAccept) {
        this._router.navigate([`${environment.languages.indexOf(lang) === -1 ? 'en' : lang}/sign-up`]);
      } else if (environment.languages.indexOf(lang) === -1) {
        this._router.navigate(['en/']);
      }
      observer.next(true);
      observer.complete();
    });
  }
}
