import { VocabularyStorageService } from './../../vocabulary-storage.service';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class VocabularyResolverService implements Resolve<any> {
  public vocabulary: any;

  constructor(
    private _http: Http,
    private _service: GeneralService,
    private _storage: VocabularyStorageService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Response> {
    const lang = route.params['lang'];
    this._service.language = lang;
    this._service.processingQueryParams(route.queryParams);

    return Observable.create(observer => {
      this._service.getVocabulary(lang)
        .subscribe(
          (vocabulary) => {
            this.vocabulary = vocabulary;
            this._storage.vocabulary = vocabulary;
            observer.next(vocabulary);
            observer.complete();
          },
          (err) => {
            console.error(err);
            observer.next([]);
            observer.complete();
          }
        );
    });
  }

}
