// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  apiUrl: 'http://api.fxbot.alpha/api',
  offer: 'a1kdailyprofit',
  offerToken: 'b7c83967867c075a22720fa6e5ede54c',
  module: 'A1kdailyprofit',
  favicon: './assets/a1kdailyprofit/images/favicons/favicon.ico',
  h1: 'Ride The Wave of bitcoin And you could earn up to $13,000 In Exactly 24 Hours',
  h2: 'Join The Bitcoin Code2',
  h3: 'Join The Bitcoin Code3',
  languages: [
    'en',
    'de',
    'se',
    'it',
    'no',
    'se'
  ],
  gaUID: 'UA-113310154-5'
};
