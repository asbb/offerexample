$(document).ready(function() {

    if ($('input[type="radio"]:checked').length) {
        $('input[type="radio"]:checked').parent("label").addClass('checked');
    }
    if ($('input[type="checkbox"]:checked').length) {
        $('input[type="checkbox"]:checked').parent("label").addClass('checked');
    }

    $('label input[type="radio"]').change(
        function(){
            var el = $(this);

            radioRender(el);
        }
    );
    $('label input[type="checkbox"]').change(
        function(){
            var el = $(this);
            var parent = el.parent('label');
            parent.toggleClass('checked');
        }
    );

    $('.to_top_button').click(function(){
        $('body, html').animate({scrollTop:0}, 500);
        return false;
    });

    $('.jv_nav li a').click(function(){
        var target = $($(this).attr('href')).offset().top-30;
        $('body, html').animate({scrollTop:target}, 500);
        return false;
    });


    loading();

    if ($('#Profit').length){
        setInterval(mkChanges,200);
    }

    $(window).trigger('resize');
    $(window).trigger('scroll');

});

$(window).on('load', (function(){
}));
$(window).resize(function() {
    scaleTitles();

    calcCounts();

    //if ($(window).width() >= 768) {
    //
    //    if($(window).width() < 1530){
    //        $('#infoscroll').css({'bottom':'15px', 'top':'', 'display':'block'});
    //    }
    //    else $('#infoscroll').css({'top':'15px', 'bottom':'', 'display':'block'});
    //}

});
$(window).scroll( function(){
    calcCounts();

    if ($(window).scrollTop() > 0) {
        $('.to_top_button').fadeIn(300);
    } else {
        $('.to_top_button').fadeOut(300);
    }
});
function rand_num(min,max) {
    var number = min + Math.floor(Math.random() * max);
    return number;
}
function rand_num2(min,max) {
    var number = min + Math.floor((Math.random() * max)/2);
    return number;
}
function numberCom(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) val = val.toString().replace(/(\d+)(\d{3})/, "$1"+","+"$2");
    return val;
}
var html;
var profit=238957514;
function mkChanges() {
    var p = rand_num(5,15);
    profit+=p;
    document.getElementById("Profit").innerHTML="$"+numberCom(profit)+"!";
}


var mousebottom = 0;
var pu = 1;
setTimeout(function(){pu=1;},5000);

$(document).mousemove(function(e){
    var X = e.pageX;
    var Y = e.pageY;

    //console.log(Y - $(window).scrollTop());
    if(Y-$(window).scrollTop() > 200)
        mousebottom = 1;

    if(Y-$(window).scrollTop()<15 && mousebottom == 1 && pu == 1){
        //$('#popup').css('display', 'block');
        mousebottom = 0;
        pu = 0;
    }

    $('a').click(function() {
        yesyoucan = 0;
        //console.log('1. ' + yesyoucan);
    });
});

var s = 1;
startnum = 49;
var awwa = false;

function loading(){

    if(s){
        s = 0;
        $('#loadblack').css({'display':'block', 'width' : 169}).animate({'width' : 1}, 1500);
        setTimeout(
            function(){
                var randoms = Math.floor((Math.random() * 3)+1);
                //console.log(randoms);
                var r = startnum - randoms;

                startnum = r<3 ? Math.floor(Math.random() * 10)+4 : r;
                $('.num').html(r);
                s = 1;
            }, 1500);//defnum = r;
    }
    awwa = setTimeout(loading,8000);
}

yesyoucan = 0;

var ajaxwork = false;
function reloadpage(){
    var currentdata = {};

    ajaxwork = $.ajax({
        url: "./exitpage-body.htm",
        success: function(bdy){
            //if(msg.answer=='ok'){

            $('body').empty();
            $('body').append(bdy);
//console.log(msg);
            //}
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });//.abort()
}

onsubmitfix = 1;

var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
function checkFormPopup2() {
    var name = $("#name2");
    if(name.val() == "") {
        alert("Please enter your name");
        name.focus();
        return false;
    }

    var email = $("#email2");
    if (!(pattern.test(email.val()))) {
        alert("Please enter a valid email");
        email.focus();
        return false;
    }

    return true;
}

function scaleTitles(){

    $('.trim_spaces').each(
        function(){
            var el = $(this);
            var target = el.find('.scalable');

            var ratio = 1;

            if (target.width() > el.width()){
                ratio = el.width()/target.width();
            }

            target.css('transform', 'scale('+ratio+')');
            el.height(target.height()*ratio);

        }
    );
}

function rand( min, max ) { // Generate a random integer
    if( max ) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    } else {
        return Math.floor(Math.random() * (min + 1));
    }
}

function calcCounts(){
    var wW = $(window).width(),
        mW = 980,
        cW = $('.count').width(),
        mM = 70;

    if (wW > 1500) {
        var newPos = (wW - mW) / 2 - cW - mM;
        var tPos = $(window).scrollTop();
    } else {
        var newPos = (wW - mW) / 2 - cW + 60;

        if ($(window).scrollTop() < 100){
            var tPos = 100;
        } else {
            var tPos = $(window).scrollTop();
        }

    }

    $('.count.left').css('left', newPos+'px');
    $('.count.right').css('right', newPos+'px');
    $('.count').css('top', tPos+'px');

}

function radioRender(el) {
    var t = el.val();
//    console.log(t);
    $('#companylogin').attr('placeholder', t+' login');
    $('#company').val(t);
    replaceafflink(el.val());
//    $('div.clicksure-field').removeClass('hide');
//    $('div.clickbetter-field').removeClass('hide');
//    $('div.'+t+'-field').addClass('hide');


    var parent = el.parent('label'),
        els = $('input[name="' + el.attr('name') + '"]');

    els.parent('label').removeClass('checked');
    els.prop('checked', false);

    parent.toggleClass('checked');
}
